# Matboj-Regelwerk

Das Regelwerk für den Matboj, der jährlich auf dem Mathecamp stattfindet.

# Dokument / Download

Die [aktuelle Version][pdf] des Regelwerks wird aus diesem Repository automatisch (mittels GitLab Runner) kompiliert.

Der [Log][log] des letzten Kompiliervorgangs kann ebenfalls eingesehen werden.

# Technisches

(Nur relevant für Betreur:innen)

Es gab mal ein Matboj repository (im Sommer 2022), das alle Materialien für den Matboj enthielt
(Aufgabensammlung etc).

Da alle Materialien des Mathezirkels mittlerweile open source sein müssen,
wurde dieses zu
[git.abstractnonsen.se/mathecamp/matboj](https://git.abstractnonsen.se/mathecamp/matboj)
migriert.
Das repository ist weiterhin privat, meldet euch bei Max, wenn Ihr Zugang braucht.

Das ist nicht schön, aber wir haben derzeit keine bessere Lösung.

Hier auf GitLab befindet sich zur Zeit nur noch das Regelwerk.

# License

The `inputs` and the provided PDF are licensed under [CC-BY-SA-4.0](https://creativecommons.org/licenses/by/4.0/), the rest is MIT. See [LICENSE.md](LICENSE.md) for further details.
In particular, feel free to use and share the PDF to your liking.

[pdf]: https://mathezirkel.gitlab.io/content/matboj-regelwerk/Matboj_Regelwerk.pdf
[Log]: https://mathezirkel.gitlab.io/content/matboj-regelwerk/Matboj_Regelwerk.log
