Different licenses apply to different parts of this repository

1. All files in the `inputs` folder are licensed unter the Creative Commons Share-Alike 4.0 international license.
2. The compiled pdf, obtained by running `make`, is also licensed under the Creative Commons Share-Alike 4.0 international license
3. All other files in this repository (not in the `inputs` folder) are licensed under MIT.

You can find the corresponding licenses in the subfolder `licenses`
